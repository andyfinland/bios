; Load hex bytes
; load <address>
;
; -> [HEX (ascii)] CR/LF
; -> EOL - 10h ; Control-P

isLOAD:
    ld de, CommandLOAD
    ld hl, CMDBUF
    call strcmp
    call z, loadCommand
    ret

; Check if there is an address
loadCommand:
    ld hl, CMDBUF
    call strlen                   ; return A
    ld d, a
    ld hl, CommandLOAD
    ld a, (hl)
    cp d
    jr nz, loadParam              ; yes there are params

; Otherwise set starting address
loadDefault:
    ld hl, APPSPC
    jp loadBytes

loadParam:
    ld iy, CMDBUF+5              ; starting point for address
    call hexAsciiTo16dec         ; return HL

loadBytes:
    push hl
    ld hl, startByteStr
    call printStr
    pop hl
loopBytes:
    call getChr                   ; get first character
    cp CR
    jr z, exitGetByteRow
    cp LF
    jr z, exitGetByteRow
    cp EOL
    jr z, exitGetBytes
    call convertHexToByte
    ld (hl), a
    inc hl
    jp loopBytes

exitGetByteRow:
    ld a, '.'
    call printChr
    jp loopBytes

exitGetBytes:
    ld hl, doneByteStr
    call printStr
    ret

convertHexToByte:
    ld d, a
    call getChr             ; get next part of hex
    ld e, a
    call hexAsciiTo8Dec     ; get dec returns A
    ret;
