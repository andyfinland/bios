#!/bin/bash

# App starts at 0x0000
# Syntax: build.sh <project path> 

# sh <path to build file>/build.sh <path to project>

# clean up

if test -f "$1/main.bin"; then rm $1/main.bin; fi
if test -f "$1/main.rom"; then rm $1/main.rom; fi
if test -f "$1/main.lst"; then rm $1/main.lst; fi

# set dir
cd $1/

# build
/Users/andrew/Dev/Retro/zasm/zasm  --z80 -uv2wyb $1/main.asm

if test -f "$1/main.txt"; then rm $1/main.txt; fi
if test -f "$1/main.byte"; then rm $1/main.byte; fi
if test -f "$1/main.hex"; then rm $1/main.hex; fi
if test -f "$1/main.dis"; then rm $1/main.dis; fi

# create bin
mv $1/main.rom $1/main.bin

# Convert to hex
/Users/andrew/Dev/Retro/z88dk/bin/z88dk-appmake +hex --org 0x0000 -b $1/main.bin -o $1/main.hex

# Convert to bytes
/usr/bin/hexdump -v -e ' 1/1 "%02X" ' $1/main.bin > $1/main.byte	

# Convert to binary code
/usr/bin/xxd -g1 -u $1/main.bin > $1/main.txt

# Disassemble
/Users/andrew/Dev/Retro/asz80/bin/dz80 /M=0 /S=0 /L "main.bin" "main.dis"