; Z80 Emulator

; ROM 0x0000 - 0x7FFF 0-32K
; RAM 0x8000 - 0xFFFF 32K-64K

ACIAD   .equ    01h

; Init
.org 0000h
    jp init

; Print character
; Input A
.org 08h
RST08:
    jp printChr

; Print string
; Input HL, end EOS
.org 10h
RST10:
    jp printStr
    
; Get character
; Output: A
.org 18h
RST18:
    jp getChr

; Get string
; Output: HL
.org 20h
RST20:
    jp getStr

; Interrupt mode 1
.org 0038h
RST38:
    reti

; NMI Interupt
.org 0066h
RST66:
    retn

; BIOS resume
.org 0080h
RST80:
    jp resume
    
; Initalise BIOS
.org 0100h
init:
    jp clearRAM
initStack:
    ld sp, STACK
    jp start
    
; Get character
; Output: A
getChr:
    in a, (ACIAD)
    ret

; Print char
; Input A
printChr:
    out (ACIAD),a
    ret


