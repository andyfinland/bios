;  ----- Dump memory contents as HEX -----
isDUMP:
    ld de, CommandDUMP
    ld hl, CMDBUF
    call strcmp
    call z, dumpCommand
    ret

; Check if there is an address
dumpCommand:
    ld hl, CMDBUF
    call strlen                   ; return A
    ld d, a
    ld hl, CommandDUMP
    ld a, (hl)
    cp d
    jr nz, dumpParam              ; yes there are params

; Otherwise set starting address
dumpDefault:
    ld hl, APPSPC
    jp dumpRow

; get starting address
dumpParam:
    ld iy, CMDBUF+5              ; starting point for address
    call hexAsciiTo16dec         ; return HL

; loop address rows
dumpRow:
    ld b, HEXROW
dumpRowLoop:
    push bc
    call printAddress
    push hl
    call dumpCol
    pop hl
    call asciiCol
    pop bc
    call crlf
    DJNZ dumpRowLoop
    ld a, EOT           ; send EOT character
    call printChr
    ret

; loop hex cols
dumpCol:
    ld b, HEXCOL
dumpColLoop:
    ld a, (hl)
    push hl
    push de
    call decToHexAscii
    call dumpHex
    pop de
    pop hl
    inc hl
    dec d
    DJNZ dumpColLoop
    ret

; loop text cols
asciiCol:
    ld b, HEXCOL
asciiColLoop:
    ld a, (hl)
    push hl
    push de
    call dumpText
    pop de
    pop hl
    inc hl
    dec d
    DJNZ asciiColLoop
    ret

dumpText:
    cp SPACE            ; Range check 20h..7eh
    jp m,showDot
    cp 07fh
    jp p,showDot
    call printChr
    ret
showDot:
    ld a, '.'
    call printChr
    ret

dumpHex:
    ld a, d
    call printChr
    ld a, e
    call printChr
    ld a,' '
    call printChr
    ret
