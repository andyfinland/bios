; ROM 0x0000 - 0x7FFF 0-32K
; RAM 0x8000 - 0xFFFF 32K-64K

STACK		.equ  0xFFFF	  ; Top of RAM

CMDBUF      .equ  0x8000      ; Start of command buffer
CMDSIZ      .equ  0x7F        ; Max size of command 128 bytes

STRBUF      .equ  0x8080      ; Start of string buffer
STRSIZ      .equ  0x7F        ; Command buffer 128 bytes

BIOSPX		.equ  0x8100	  ; BIOS command position

APPSPC      .equ  0x8200      ; Default start of application space
HEXCOL      .equ  16          ; Number of columns (dump)
HEXROW      .equ  8           ; Number of rows (dump)


; load serial
; ACIA = acia.asm
; Z80Emulator = z80emu.asm

#include "z80emu.asm"
#include "variables.asm"
#include "utils.asm"
#include "commands.asm"
#include "dump.asm"
#include "ihex.asm"
#include "load.asm"
#include "run.asm"
#include "call.asm"

; Start BIOS
start:
    ld hl, bios
    call printStr
resume:
    ld hl, prompt
    call printStr
waitForChar:
    call commands
    jp waitForChar
    
; Commands
commands:

    call getCommand 

    call isREADY
    call isDIR
    call isHELP
    call isDUMP
    call isIHEX
    call isRUN
    call isCALL
    call isRESET
    call isLOAD

    ld hl, prompt
    call printStr

    ret

; Get command input
; Output - CMDBUF
getCommand:
    call clearCMDBUF
    call clearBIOSPX
    ld hl, CMDBUF
    
loopGetCommand:
    call getChr
    
    ; handle keys
    cp BKSP
    jp z, handleBackSpace
    
    ; echo char
    call printChr
    
    ; wait for CR/LF
    cp CR
    jp z, getCommandExit
    cp LF
    jp z, getCommandExit
    
    ; add to cmd buffer
    ld (hl), a
    inc hl
    
    ; increment cursor position
    ld de, BIOSPX
    ld a, (de)
    inc a
    ld (de),a
    
    ; check buf met
    cp CMDSIZ
    jp z, moveBackSpace
    
    djnz  loopGetCommand
    
handleBackSpace:
	; check if pos at 0 (cursor)
	ld de, BIOSPX
	ld a, (de)
	cp 0
	jp z, loopGetCommand

moveBackSpace:
	; dec pos
    dec a
    ld (de),a
    
    push hl
	push bc
	
	; move cursor left
	ld hl, moveLeft
	call printStr
	ld a, ' '
	call printChr
	ld hl, moveLeft
	call printStr
	
	pop bc
	pop hl
	
	; remove from cmd buffer
    ld (hl), a
    dec hl

	jp loopGetCommand
	
getCommandExit:
    ld a, EOS     ; at eos marker for printStr
    ld (hl), a
    ret

; Get string input
; Output - STRBUF
getStr:
    call clearSTRBUF
    ld hl, STRBUF
loopGetStr
    call getChr
    call printChr
    cp CR
    jr z, getStrExit
    cp LF
    jr z, getStrExit
    ld (hl), a
    inc hl
    djnz  loopGetStr
getStrExit:
    ld a, EOS
    ld (hl), a       ; at eos marker for printStr
    ret

; Print string
; Input: HL
; Terminates EOS
printStr:
    ld a, (hl)
    cp EOS
    jr z,printdone
    call printChr
    inc hl
    jp printStr
printdone:
    ret
    
; Clear string buffer
; Input STRBUF (HL)
clearSTRBUF:
    ld hl,STRBUF
    ld e,l
    ld d,h
    inc de
    ld (hl),00h
    ld bc,STRSIZ
    ldir
    ret

; Clear command CMDBUF
; Input CMDBUF (HL)
clearCMDBUF:
    ld hl,CMDBUF
    ld e,l
    ld d,h
    inc de
    ld (hl),00h
    ld bc,CMDSIZ
    ldir
    ret
 
; Clear position
clearBIOSPX:
	ld de, BIOSPX
	ld a, 0
	ld (de), a
    ret

; clear RAM
clearRAM:
    ld hl,$8000
    ld e,l
    ld d,h
    inc de
    ld (hl),$00
    ld bc,$8000
    ldir
    jp initStack

end:
    halt
