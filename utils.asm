; Prints 16bit number as decimal ASCII
; Input: HL
; Output ASCII (padded e.g. 00200)
print16decToAscii:
	ld	bc,-10000
	call	Dec16ASC1
	ld	bc,-1000
	call	Dec16ASC1
	ld	bc,-100
	call	Dec16ASC1
	ld	c,-10
	call	Dec16ASC1
	ld	c,-1
Dec16ASC1:
  ld	a,'0'-1
Dum16ASC2:
  inc	a
	add	hl,bc
	jr	c, Dum16ASC2
	sbc	hl,bc
  call printChr
	ret

; Prints prefix hex '0x'
prefixHEX:
    ld a, '0'
    call printChr
    ld a, 'x'
    call printChr
    ret

; Prints address as ascii
; Input: HL contains address to be printed
; Output: Prints ASCII
print16ToHexAscii:
    ld a,h
    srl a
    srl a
    srl a
    srl a
    cp a,10
    jp m,h3digit
    add a,7
h3digit:
    add a,30h
    call printChr
    ld a,h
    and a,0fh
    cp a,10
    jp m,h4digit
    add a,7
h4digit:
    add a,30h
    call printChr
    ld a,l              ; And again with the low half
    srl a
    srl a
    srl a
    srl a
    cp a,10
    jp m,h5digit
    add a,7
h5digit:
    add a,30h
    call printChr
    ld a,l
    and a,0fh
    cp a,10
    jp m,h6digit
    add a,7
h6digit:
    add a,30h
    call printChr
    ret

; Convert decimal to Ascii HEX (8 bit)
; Input: A = number to convert
; Return: DE (ascii)
decToHexAscii:
    ld c, a
    call Num1
    ld d, a
    ld a, c
    call Num2
    ld e, a
    ret
Num1:
    rra
    rra
    rra
    rra
Num2:
    or $F0
    daa
    add a, $A0
    adc a, $40 ; Ascii hex at this point (0 to F)
    ret

 ; Print HEX
 ; Input DE
print8bitHex:
    ld a, d
    call printChr
    ld a, e
    call printChr
    ld a,' '
    call printChr
    ret
    
; Convert Ascii HEX (XXh) to Decimal (8 bit)
; Input DE = hex string
; Return: A
hexAsciiTo8Dec:
    ld   a,d
    call Hex1
    add  a,a
    add  a,a
    add  a,a
    add  a,a
    ld   d,a
    ld   a,e
    call Hex1
    or   d
    ret
    
Hex1:
    sub  a,'0'
    cp   10
    ret  c
    sub  a,'A'-'0'-10
    ret

; Convert Acsii HEX (XXXXh) to Decimal (16bit)
; Input: Address of ascii (iy)
; Return: HL
hexAsciiTo16dec:
    ld hl, 0
    ld d, (iy)    ; jump to byte
    ld e, (iy+1)
    call hexAsciiTo8Dec    ; return A
    ld  h, a
    ld d, (iy+2)    ; jump to byte
    ld e, (iy+3)
    call hexAsciiTo8Dec    ; return A
    ld  l, a
    ret

; Prints 8bit Number in A as decimal ASCII
; Input: A = number
; Output: ASCII (padded 056)
; Destroys BC, HL
print8bitDec2asciiXXX:
	ld	c,-100
	call	Na1
print8bitDec2asciiXX:
	ld	c,-10
	call	Na1
	ld	c,-1
Na1:	
	ld	b,'0'-1
Na2:	
	inc	b
	add	a,c
	jr	c,Na2
	sub	c				;works as add 100/10/1
	push af				;safer than ld c,a
	ld	a,b				;char is in b
	call printChr		
	pop af				;safer than ld a,c
	ret
	
; Copy memory
; Input: HL=from, DE=to, BC=num bytes
copyMem:
    push de
    push hl
    push bc
loopMem:
    ldir
    jp nz, loopMem
    pop bc
    pop hl
    pop de
    ret

; Covert to uppercase
; Input - A
; Output - A
convertToUpper:
    cp 0x61
    ret c
    cp 0x7b
    ret nc
    sub 0x20
    ret
    
; Compare Strings
; Input: HL=String1, DE=String2
; Output: flags nz = no match, z = match, HL/DE = location of end of string
strcmp:
    ld a, (de)  ; get string length from byte in address
    ld b, a     ; get length
    inc de      ; jump to string byte
strcmploop:
    ld a,(de)
    cp (hl)
    ret nz      ; no match
    inc hl
    inc de
    djnz strcmploop
    ret z       ; match

; Get string length
; input hl
; return A - length
strlen:
    ld b, 0
StrLen00:
  	ld a, (hl)
  	cp EOS
  	jr z, StrLen01
  	inc hl
  	inc b
	  jr StrLen00
StrLen01:
  	ld a, b
  	ret
	
; Print CRLF
crlf:
    ld a, CR
    call printChr
    ld a, LF
    call printChr
    ret

; print hex address as ascii
; input - HL
printAddress:
    call print16ToHexAscii
    ld a, ':'
    call printChr
    ld a, ' '
    call printChr
    ret