
# Rom 0x0000 - 0x07FF 0-2K
# ROM 0x0000 - 0x7FFF 0-32K
# RAM 0x8000 - 0xFFFF 32K-64K

FILE=main
ZASM = ~/z80editor2/zasm
APPMAKE = ~/z80editor2/z88dk-appmake
HEXDUMP = ~/z80editor2/hexdump
STARTADDRESS = 0x0000

include ~/Dev/makefile/asm/makefile-asm-z80editor
