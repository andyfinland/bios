; ----- Call ------

isCALL:
    ld de, CommandCALL
    ld hl, CMDBUF
    call strcmp
    call z, callCommand
    ret

; Check if there is an address
callCommand:
    ld hl, CMDBUF
    call strlen                   ; return A
    ld d, a
    ld hl, CommandCALL
    ld a, (hl)
    cp d
    jr nz, callParam              ; yes there are params

; Otherwise set starting address
callDefault:
    ld hl, APPSPC
    call callApp
    ret

callParam:
    ld iy, CMDBUF+5              ; starting point for address
    call hexAsciiTo16dec         ; return HL
    call callApp
    ret

callApp:
    jp (hl)
    ret