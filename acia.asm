; ACIA Uart   
; ROM 0x0000 - 0x7FFF 0-32K
; RAM 0x8000 - 0xFFFF 32K-64K

ACIAS   .equ    0x80        ; 6850 ACIA status register 	(10000000)
ACIAD   .equ    0x81        ; 6850 ACIA control register	(10000001)

ACIRS   .equ    0x03        ; 6850 Reset the ACIA			(00000011)
ACIST   .equ    0xD6		; Set RTS high int disabled		(11010110)

							; RTS low int dis (96h)			(10010110)
							; RTS high int dis (D6h)    	(11010110)
 
; Init
.org 0000h
    jp init

; Print character
; Input A
.org 08h
RST08:
    jp printChr

; Print string
; Input HL, end EOS
.org 10h
RST10:
    jp printStr
    
; Get character
; Output: A
.org 18h
RST18:
    jp getChr

; Get string
; Output: HL
.org 20h
RST20:
    jp getStr

; Interrupt mode 1
.org 0038h
RST38:
    reti

; NMI Interupt
.org 0066h
RST66:
    retn
    
; BIOS resume
.org 0080h
RST80:
    jp resume

; Initalise BIOS
.org 0100h
init:
    jp clearRAM
initStack:
    di
    ld sp, STACK
    call initSerial
    jp start

; Print character
; Input - A
printChr:
    push bc
    ld c,a
isTxReady:
    in a,(ACIAS)
    bit 1,a
    jr Z,isTxReady
    ld a,c
    out (ACIAD),a
    pop bc
    ret

; Get character
; Output - A
getChr:
isRxReady:
    in a, (ACIAS)
    bit 0,a
    jr z,isRxReady
    in a, (ACIAD)
    ret

; Reset 68B50
initSerial:
    ld a, ACIRS            ; RESET the ACIA
    out (ACIAS), A
    ld a, ACIST           ; setup the ACIA
    out (ACIAS), A
    ret
