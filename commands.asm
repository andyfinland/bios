; Commands

;  ----- Ready -----
isREADY:
    ld de, CommandREADY
    ld hl, CMDBUF
    call strcmp
    jp z, readyCommand
    ret

readyCommand:
    ld hl, ok
    call printStr
    ret

;  ----- Reset -----
isRESET:
    ld de, CommandRESET
    ld hl, CMDBUF
    call strcmp
    jp z, resetCommand
    ret

resetCommand:
    jp init

;  ----- Help Menu -----
isDIR:
    ld de, CommandDIR
    ld hl, CMDBUF
    call strcmp
    call z, showHelp
    ret

isHELP:
    ld de, CommandHELP
    ld hl, CMDBUF
    call strcmp
    call z, showHelp
    ret

showHelp:
    call crlf
    ld hl, dir
    call printStr
    ret


