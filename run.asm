; ----- Run ------

isRUN:
    ld de, CommandRUN
    ld hl, CMDBUF
    call strcmp
    jp z, runCommand
    ret

; Check if there is an address
runCommand:
    ld hl, CMDBUF
    call strlen                   ; return A
    ld d, a
    ld hl, CommandRUN
    ld a, (hl)
    cp d
    jr nz, runParam              ; yes there are params

; Otherwise set starting address
runDefault:
    ld hl, APPSPC
    jp hl

runParam:
    ld iy, CMDBUF+4              ; starting point for address
    call hexAsciiTo16dec         ; return HL
    jp hl
