;  ----- Load HEX into memory -----
; ihex
; -> [Intel HEX (ascii)] CR/LF
; -> :00000001FF (CR/LF)

isIHEX:
    ld de, CommandIHEX
    ld hl, CMDBUF
    call strcmp
    call z, ihexCommand
    ret

; Load HEX into address
; Input: HL = start address
ihexCommand:
    ld hl, startHEXstr
    call printStr
ihexMore:
    call getStr
    call ihexLoad
    jr z, ihexCompleted
    jp ihexMore
ihexCompleted:
    ld hl, doneHEXstr
    call printStr
    ret

;  ----- Load Intel HEX into memory -----

; : is start of line marker
; BB is number of data bytes on line
; AAAA is address in bytes
; TT is type discussed below but 00 means data
; DD is data bytes, number depends on BB value
; CC is checksum (2s-complement of number of bytes+address+data)

; Type 1=EOF, 0=Data

ihexLoad:

    ; get type from ascii hex
    ld iy, STRBUF+7
    ld d, (iy)
    ld e, (iy+1)
    call hexAsciiTo8Dec
    cp 1
    ret z                     ; return if type=1

    ; get address from (ascii) address
    ld iy, STRBUF+3
    call hexAsciiTo16dec     ; input iy, return HL

    ; get num of bytes from (ascii) bytes
    ld iy, STRBUF+1
    ld d, (iy)
    ld e, (iy+1)
    call hexAsciiTo8Dec       ; input DE, return A
    ld b, a                   ; B holds number of bytes

    ; loop num of bytes (b)
    ld iy, STRBUF+9     ; start of HEX
    ld b, a

iHEXloop:
    ld d, (iy)
    ld e, (iy+1)
    call hexAsciiTo8Dec
    ld (hl), a
    inc hl
    inc iy
    inc iy
    djnz iHEXloop
    ret nz
