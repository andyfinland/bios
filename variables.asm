; Variables

; Default
CR      EQU 0Dh 
LF      EQU 0Ah 
EOS     EQU 00h 
EOL		EQU 10h ; Control-P
SPACE   EQU 20h
BKSP	EQU	7Fh	; Backspace 7Fh (Mac) / 08h
ESC 	EQU 1Bh;

; XMODEM
NUL:    EQU 00h ; Control @
SOH:	EQU	01h	; Control A
STX:    EQU 02h ; Control B
ETX:    EQU 03h ; Control C
EOT:	EQU	04h	; Control D = End of Transmission
ACK:	EQU	06h	; Control F = Positive Acknowledgement
NAK:	EQU	21h	; Control U = Negative Acknowledgement
CAN:	EQU	24h	; Control X = Cancel
BEL:    EQU 07h ; Control G

; VT100 codes
CLS     .DB ESC,"[2J", EOS
HOME    .DB ESC,"[H", EOS

; Commands
CommandREADY:       .DB 5,"ready"
CommandDIR:         .DB 3,"dir"
CommandHELP:        .DB 4,"help"
CommandLOAD:        .DB 4,"load"
CommandDUMP:        .DB 4,"dump"
CommandIHEX:        .DB 4,"ihex"
CommandRUN:         .DB 3,"run"
CommandCALL:        .DB 4,"call"
CommandRESET:       .DB 5,"reset"

; Text
bios:         .DB ESC,"[2J",27,"[H","AW BIOS 1.2", CR, LF 
			  .DB "Apr 2022", CR, LF, CR, LF, EOS
prompt:       .DB ">", EOS
ok:           .DB "OK", CR, LF, EOS

startHEXstr:   .DB "Start HEX transfer:", CR, LF, EOS
doneHEXstr:    .DB "HEX transfer completed!", CR, LF, EOS
startByteStr:  .DB "Start byte transfer (CR + Ctrl-D):", CR, LF, EOS
doneByteStr:   .DB CR, LF,"Byte transfer completed!", CR, LF, EOS

; Byte Transfer
xAck:       .DB 'ACK',CR, LF, EOS
xEnd:       .DB 'DONE',CR, LF, EOS

; Menu
dir:
    .DB "A: ready", CR, LF
    .DB "A: help | dir", CR, LF
    .DB "A: dump <address>", CR, LF
    .DB "A: ihex", CR, LF
    .DB "A: load <address>", CR, LF
    .DB "A: run <address>", CR, LF
    .DB "A: call <address>", CR, LF
    .DB "A: reset", CR, LF, CR, LF, EOS

; ------- Navigation -------

moveDown:	.DB	ESC, "[1B", EOS
moveUp:		.DB	ESC, "[1A", EOS
moveRight:	.DB	ESC, "[1C", EOS
moveLeft:	.DB	ESC, "[1D", EOS

